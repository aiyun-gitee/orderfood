# orderfood

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```



前端地址：https://yk.52kfw.cn/index.html#/home



后端地址：https://yk.52kfw.cn/admin.html



接口地址：https://api.52kfw.cn/api.html



